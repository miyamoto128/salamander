from PyQt5 import QtWidgets, uic
from constants import ABOUT_DIALOG_UI


class AboutDialog(QtWidgets.QDialog):
    def __init__(self, parent):
        super(AboutDialog, self).__init__(parent)
        uic.loadUi(ABOUT_DIALOG_UI, self)
