from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import Qt, QUrl
from PyQt5.QtGui import QTextCursor
from PyQt5.QtWebEngineWidgets import QWebEngineView
from app.http import Requester, RequestBuilder, Request, Response
from constants import MAIN_WINDOW_UI
from data.history import HistoryMapper, History as DbHistory
from ui.about_dialog import AboutDialog
import json


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, db_session):
        super(MainWindow, self).__init__()

        uic.loadUi(MAIN_WINDOW_UI, self)
        self.about_dialog = AboutDialog(self)

        self.db_history_mapper = HistoryMapper(db_session)
        self.current_req = False
        self.preview_loaded = False

        self.__init_slots()
        self.__init_widgets()
        self.__init_menus()

        self.show()

        self.__history_load()

    def __init_slots(self):
        self.exit_action.triggered.connect(self.close)
        self.about_action.triggered.connect(self.__slot_about_triggered)
        self.history_tree.itemClicked.connect(self.__slot_history_clicked)
        self.req_method.currentTextChanged.connect(self.__slot_method_changed)
        self.req_send.clicked.connect(self.__slot_send_request)
        self.resp_tabs.currentChanged.connect(self.__slot_resp_tab_changed)

        self.history_group.clicked.connect(self.__slot_history_group_clicked)
        self.req_group.clicked.connect(self.__slot_req_group_clicked)
        self.resp_group.clicked.connect(self.__slot_resp_group_clicked)

    def __init_widgets(self):
        self.history_group.hide()
        self.resp_group.hide()
        self.req_body_tab.setEnabled(False)

        self.__add_preview_widget()

    def __init_menus(self):
        self.history_send_action = QtWidgets.QAction("Send", None)
        self.history_send_action.triggered.connect(self.__slot_send_request)
        self.history_delete_action = QtWidgets.QAction("Delete", None)
        self.history_delete_action.triggered.connect(self.__slot_history_delete)

        self.history_tree.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.history_tree.addAction(self.history_send_action)
        self.history_tree.addAction(self.history_delete_action)

    def __add_preview_widget(self):
        self.resp_preview = QWebEngineView(self)
        self.resp_preview.setMinimumHeight(300)
        self.resp_preview_tab.layout().addWidget(self.resp_preview)

    def __slot_about_triggered(self):

        self.about_dialog.show()


    def __slot_history_clicked(self, item):
        self.__enable_requests(False)

        _, req, resp = item.data(0, Qt.UserRole)
        self.req_method.setCurrentText(req.method)
        self.req_url.setText(req.url)
        self.__set_req_headers(req.headers)
        self.req_body.clear()
        self.req_body.appendPlainText(req.body)

        self.__render_response(resp)

        self.current_req = req

        self.__enable_requests(True)
        self.resp_group.show()

    def __slot_history_delete(self):
        current_item = self.history_tree.currentItem()
        item_id, _, _ = current_item.data(0, Qt.UserRole)
        index = self.history_tree.indexOfTopLevelItem(current_item)

        if self.__db_history_remove(item_id):
            self.history_tree.takeTopLevelItem(index)

    def __slot_method_changed(self, method):
        if Requester.has_body(method):
            self.req_tabs.setCurrentWidget(self.req_body_tab)
            self.req_body_tab.setEnabled(True)
        else:
            self.req_body_tab.setEnabled(False)

    def __slot_send_request(self):
        self.__enable_requests(False)

        self.resp_group.show()
        self.resp_status.setText("Waiting for response ...")

        headers = [(self.req_headers.item(n, 0).text().strip(), self.req_headers.item(n, 1).text().strip())
                   for n in range(0, self.req_headers.rowCount())
                   if self.req_headers.item(n, 0) and self.req_headers.item(n, 1)]
        headers = dict([(k, v) for (k, v) in headers if k and v])

        builder = RequestBuilder()
        req = builder.set_method(self.req_method.currentText()) \
            .set_url(self.req_url.text().strip()) \
            .set_headers(headers) \
            .set_body(self.req_body.toPlainText()) \
            .create()

        resp = Requester.send(req)
        item_id = self.__db_history_add(req, resp)
        self.__history_add(item_id, req, resp)
        self.__render_response(resp)

        self.current_req = req

        self.__enable_requests(True)

    def __slot_resp_tab_changed(self, i):
        if i == 2 and self.current_req and not self.preview_loaded:
            self.resp_preview.load(QUrl(self.current_req.url))
            self.preview_loaded = True

    def __slot_history_group_clicked(self):
        self.__toggle_groupbox(self.history_group)

    def __slot_req_group_clicked(self):
        self.__toggle_groupbox(self.req_group)

    def __slot_resp_group_clicked(self):
        self.__toggle_groupbox(self.resp_group)

    @staticmethod
    def __toggle_groupbox(groupbox):
        children = [child for child in groupbox.children()
                    if not type(child) in [QtWidgets.QBoxLayout, QtWidgets.QVBoxLayout]]

        for child in children:
            if groupbox.isChecked():
                child.show()
            else:
                child.hide()

    def __enable_requests(self, enable):
        self.req_group.setEnabled(enable)
        self.resp_group.setEnabled(enable)

    def __set_req_headers(self, headers):
        self.req_headers.clearContents()
        y = 0
        for name, value in headers.items():
            self.req_headers.setItem(y, 0, QtWidgets.QTableWidgetItem(str(name)))
            self.req_headers.setItem(y, 1, QtWidgets.QTableWidgetItem(str(value)))
            y += 1

    def __render_response(self, resp):
        if self.req_method.currentText() == "HEAD":
            self.resp_tabs.setCurrentWidget(self.resp_headers_tab)
        else:
            self.resp_tabs.setCurrentWidget(self.resp_body_tab)
            
        if resp.status == 0:
            self.resp_status.setText("No response")
        else:
            self.resp_status.setText(f"Received code {resp.status} in {resp.elapsed} ms")

        self.resp_headers.clear()
        for k, v in resp.headers.items():
            self.resp_headers.appendPlainText(f"{k}: {v}")

        self.resp_body.clear()
        encoding = resp.encoding if resp.encoding else "UTF-8"
        self.resp_body.appendPlainText(resp.body.decode(encoding))
        self.resp_body.moveCursor(QTextCursor.Start)

        self.preview_loaded = False

    def __history_load(self):
        urls = set()

        for item in self.db_history_mapper.all():
            urls.add(item.url)
            req = Request(
                item.method,
                item.url,
                json.loads(item.headers),
                item.body,
            )
            resp = Response(
                item.resp_status,
                json.loads(item.resp_headers),
                item.resp_encoding,
                item.resp_body,
                item.resp_elapsed,
            )
            self.__history_add(item.id, req, resp, False)

        # Make use history URL for auto-completion
        self.req_url.setCompleter(QtWidgets.QCompleter(urls))

    def __history_add(self, item_id, req, resp, top=True):
        self.history_group.show()

        tooltip = f"{req.method} {req.url} -> {resp.status}"

        item = QtWidgets.QTreeWidgetItem()
        item.setText(0, req.method)
        item.setToolTip(0, tooltip)
        item.setData(0, Qt.UserRole, (item_id, req, resp))
        item.setText(1, req.url)
        item.setToolTip(1, tooltip)
        if top:
            self.history_tree.insertTopLevelItem(0, item)
        else:
            self.history_tree.addTopLevelItem(item)

    def __db_history_add(self, req, resp):
        return self.db_history_mapper.add(DbHistory(
            method=req.method,
            url=req.url,
            headers=json.dumps(req.headers),
            body=req.body,
            resp_status=resp.status,
            resp_encoding=resp.encoding,
            resp_headers=json.dumps(dict(resp.headers)),
            resp_body=resp.body,
            resp_elapsed=resp.elapsed,
        ))

    def __db_history_remove(self, item_id):
        return self.db_history_mapper.remove(item_id)
