Salamander is Graphical HTTP client, inspired by Rested and Postman.
It supports the all current HTTP methods, also editable request headers and body.

## Getting start

This project uses poetry for dependencies management.

To install poetry, follow the official guideline: https://python-poetry.org/docs/#installation.

Then, upgrade the dependencies:
```
poetry install
```

## Execute

```
python3 main.py
``` 

## Developments

To update resources images:
```
pyrcc5 salamander.qrc -o images.py
```
