from sqlalchemy import Column, Integer, Float, String, TEXT, JSON, LargeBinary, DateTime, func, desc
from sqlalchemy.ext.declarative import declarative_base
from app import utils

Base = declarative_base()


class History(Base):
    __tablename__ = 'history'
    id = Column(String(100), primary_key=True)
    method = Column(String)
    url = Column(String)
    headers = Column(JSON)
    body = Column(TEXT)
    resp_status = Column(Integer)
    resp_headers = Column(JSON)
    resp_encoding = Column(String)
    resp_body = Column(LargeBinary)
    resp_elapsed = Column(Integer)
    created = Column(DateTime, default=func.now())


class HistoryMapper:

    def __init__(self, session):
        self.session = session

    def all(self, limit=100):
        return self.session.query(History).order_by(desc(History.created)).limit(limit)

    def add(self, item):
        item.id = utils.generate_token()
        self.session.add(item)
        self.session.commit()
        return item.id

    def remove(self, id):
        item = self.session.query(History).filter_by(id=id).first()
        self.session.delete(item)
        self.session.commit()
        return True
