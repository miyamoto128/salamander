import hashlib
import time
import random


def generate_token():
    m = hashlib.sha256()
    m.update(time.time_ns().to_bytes(16, byteorder='big'))
    m.update(random.randrange(10000).to_bytes(4, byteorder='big'))
    return m.hexdigest()
