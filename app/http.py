import requests
import time
import logging
from dataclasses import dataclass, field

logger = logging.getLogger(__name__)


class Requester:
    METHODS_WITH_BODY = {"POST", "PUT", "PATCH", "DELETE"}

    @staticmethod
    def send(request):
        logger.info(f"Sending request -> {request}")

        start = time.process_time()
        try:
            r = requests.request(request.method, request.url, headers=request.headers, data=request.body, verify=False)
            elapsed = round((time.process_time() - start) * 1000, 2)
            return Response(r.status_code, r.headers, r.encoding, r.content, elapsed)
        except Exception as e:
            logger.error(str(e))

        return Response()

    @staticmethod
    def has_body(method):
        return method in Requester.METHODS_WITH_BODY


class RequestBuilder:
    def __init__(self):
        self.method = "GET"
        self.url = ""
        self.headers = {}
        self.body = None

    def set_method(self, method):
        self.method = method
        return self

    def set_url(self, url):
        self.url = RequestBuilder.__adjust_url(url)
        return self

    def set_headers(self, headers: dict):
        self.headers = headers
        return self

    def set_body(self, body):
        self.body = body
        return self

    def create(self):
        return Request(self.method, self.url, self.headers, self.body)

    @staticmethod
    def __adjust_url(url):
        if not (url.startswith("http://") or url.startswith("https://")):
            return "http://" + url
        return url


@dataclass
class Request:
    method: str
    url: str
    headers: dict
    body: str


@dataclass
class Response:
    status: int = 0
    headers: dict = field(default_factory=dict)
    encoding: str = ""
    body: bytes = b""
    elapsed: int = 0
