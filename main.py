#! /usr/bin/python3


from PyQt5 import QtWidgets
from ui.main_window import MainWindow
import sys
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from constants import DB_URL
import images


def init_db(db_url):
    engine = create_engine(db_url)
    Session = sessionmaker(bind=engine)
    session = Session()

    from data.history import History
    History.metadata.create_all(engine)

    return session


if __name__ == "__main__":
    db_session = init_db(DB_URL)

    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow(db_session)
    app.exec_()

    print("Done")
